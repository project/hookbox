
Drupal.hookbox = Drupal.hookbox || {};

$(document).ready(function () {
  Drupal.hookbox.conn = hookbox.connect(Drupal.settings.hookbox.url);
  var $doc = $(document);

  Drupal.hookbox.conn.onOpen = function() {
    $doc.trigger('onOpen.hookbox');
  }

  Drupal.hookbox.conn.onError = function(err) {
    $doc.trigger('onError.hookbox', [err]);
  }

  Drupal.hookbox.conn.onSubscribed = function(channelName, subscription) {
    var moduleParts = channelName.split('/');
    var module = moduleParts.shift();
    var channel = moduleParts.join('');

    $doc.trigger('onSubscribed.' + module, [channel, subscription]);
  }

  Drupal.hookbox.conn.onUnsubscribed = function(subscription, frame) {
    var moduleParts = subscription.channelName.split('/');
    var module = moduleParts.shift();
    var channel = moduleParts.join('');
    $doc.trigger('onUnsubscribed.' + module, [channel, subscription, frame]);
  }


})

Drupal.behaviors.hookbox = function (context) {

};
