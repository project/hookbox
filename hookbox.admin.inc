<?php

function hookbox_admin_form() {
  $form = array();

  $form['hookbox_ip'] = array(
    '#type' => 'textfield',
    '#title' => t('IP address'),
    '#default_value' => variable_get('hookbox_ip', '127.0.0.1'),
    '#size' => 15,
    '#maxlength' => 15,
    '#description' => t('The IP address of hookbox server.'),
    '#required' => TRUE,
  );

  $form['hookbox_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#default_value' => variable_get('hookbox_port', 8001),
    '#size' => 5,
    '#maxlength' => 5,
    '#description' => t('The port of hookbox server.'),
    '#required' => TRUE,
  );

  $form['hookbox_server_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Server secret'),
    '#default_value' => variable_get('hookbox_server_secret', ''),
    '#description' => t('The secret that the hookbox server will send to Drupal. Configured with -s.'),
    '#required' => TRUE,
  );

  $form['hookbox_drupal_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Drupal secret'),
    '#default_value' => variable_get('hookbox_drupal_secret', ''),
    '#description' => t('The secret that Drupal will send to the Hookbox server. Configured with -r.'),
    '#required' => TRUE,
  );

  $form['hookbox_root'] = array(
    '#type' => 'textfield',
    '#title' => t('Hookbox root'),
    '#default_value' => variable_get('hookbox_root', 'hookbox'),
    '#description' => t('This is the url that hookbox will make calls to in Drupal. Defaults to \'/hookbox\'. Configured with --cbpath.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
